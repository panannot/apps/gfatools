# gfatools Singularity container
### Package gfatools Version 0.5
gfatools is a set of tools for manipulating sequence graphs in the GFA or the rGFA format. It has implemented parsing, subgraph and conversion to FASTA/BED. More functionality may be added in future.

Homepage:

https://github.com/lh3/gfatools

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
gfatools Version: 0.5<br>
Singularity container based on the recipe: Singularity.gfatools_v0.5.def

Local build:
```
sudo singularity build gfatools_v0.5.sif Singularity.gfatools_v0.5.def
```

Get image help:
```
singularity run-help gfatools_v0.5.sif
```

Default runscript: gfatools<br>
Usage:
```
./gfatools_v0.5.sif --help
```
or:
```
singularity exec gfatools_v0.5.sif gfatools --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull gfatools_v0.5.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/gfatools/gfatools:latest

```

